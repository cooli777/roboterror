package roboterror

import "errors"

const (
	Low_lewel  = iota
	High_lewel
)

type ErrorApp struct {
	Error      error
	Message    string
	Code       int
	ToDisplay  bool
	LevelError int8
}

func Create(msgForLog string, msgForShowUser string, code int, showItMsgToDisplay bool, levelError int8) *ErrorApp {
	return &ErrorApp{errors.New(msgForLog), msgForShowUser, code, showItMsgToDisplay, levelError}
}

func CreateWithError(error error, msgForShowUser string, code int, showItMsgToDisplay bool, levelError int8) *ErrorApp {
	return &ErrorApp{error, msgForShowUser, code, showItMsgToDisplay, levelError}
}
